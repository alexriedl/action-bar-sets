## Interface: 11200
## Title: |cFF3333FFAction Bar Sets|r|cFFFF0000 v1.00|r
## Notes: Help manage multiple action bar configurations
## Version: 1.0
## SavedVariablesPerCharacter: ActionBarSets_SavedSets,ActionBarSets_SetStore
#@no-lib-strip@
Libs\Libs.xml
#@end-no-lib-strip@

# Helper Scripts
General.lua

# Main Action Bar Sets
ActionBarSets.lua
