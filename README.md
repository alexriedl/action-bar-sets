# Action Bar Sets

This is a simple addon to help save and restore ActionBar layouts. This
specifically helps those that need different spells or macros when they are
doing different activities during WoW. Basic example would be to have a set for
Raiding and another one for solo grinding. ActionBarSets are saved per
character

# How to Use
* `/abs save <NAME>` - Save your current ActionBar layout to the name specified
* `/abs load <NAME>` - Load the saved ActionBarSet and change your current layout to the one that is being loaded
* `/abs delete <NAME>` - Delete the saved ActionBarSet. This does not affect which actions are currently on your action bars
* `/abs list` - List the names of each of the ActionBarSets currently saved
* `/abs clear` - Clear out your action bars






# How to contribute

## SVN Submodules
Many wow libraries are stored in svn instead of git. To make things slightly
easier to deal with and to ensure we have consistent code, we clone the svn
repo into a git repo and make that repo a submodule of this project. Below is a
short overview of how to add/update libraries. Make sure you have a clean repo
before starting the following steps.

You will need git-svn installed to be able to run some of the below commands

### Add New Library
Source: http://fredericiana.com/2010/01/12/using-svn-repositories-as-git-submodules/

1. Modify `.git/config` by adding a section for the new library
```
[svn-remote "libstub"]
  url = https://repos.wowace.com/wow/libstub
  fetch = trunk:refs/remotes/libstub-svn
```
The fetch lines is <svn branch name>:refs/remotes/<local svn repo name>
2. Fetch the code for the library by running: `git svn fetch libstub`. To reduce download time, if you know the latest revision you can do  `git svn fetch libstub -r108:HEAD`
3. Make a git branch for the library: `git checkout -b libstub-git libstub-svn`
4. Push the new branch to the project: `git push -u origin libstub-git`
5. Back to development, and create the submodule: `git checkout master && git submodule add -b libstub-git git@gitlab.com:alexriedl/action-bar-sets.git Libs/LibStub`
6. Commit the new library: `git commit -A`

### Update Library
1. `git svn fetch libstub`
2. `git checkout libstub-git`
3. `git merge libstub-svn`
4. `git push origin HEAD`
5. `git checkout master` (or whichever branch youre working on)
6. `cd Libs/LibStub`
7. `git pull origin libstub-git`
8. `cd ../..`
9. `git add Libs/LibStub`
