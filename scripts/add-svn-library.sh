#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT_ROOT="$(realpath -m "${SCRIPT_DIR}/..")"

cd "${PROJECT_ROOT}"

SVN_REPO="${1}"
SVN_BRANCH_NAME="${2}"
LIBRARY_NAME="${3}"
LATEST_REVISION="${4}"

if [ "${1}" == "-h" ] || [ "${1}" == "--help" ] || [ -z "${SVN_REPO}" ] || [ -z "${SVN_BRANCH_NAME}" ] || [ -z "${LIBRARY_NAME}" ]; then
  echo "How to use:"
  echo "Pass in 4 parameters to clone a svn repo like a git submodule. This script assumes the git repository is in a clean state (no pending changes)"
  echo "Parameter 1: svn repo"
  echo "Parameter 2: svn branch name"
  echo "Parameter 3: Library name"
  echo "Parameter 4: Latest svn revision (optional, saves A LOT of time if there is a lot of history for the project)"
  echo "Example:"
  echo "./add-library 'https://repos.wowace.com/wow/libstub' 'trunk' 'LibStub' 'r128'"
  exit 0
fi

CURRENT_BRANCH_NAME="$(git symbolic-ref HEAD 2>/dev/null | cut -d"/" -f 3)"

LOWER_LIBRARY_NAME="${LIBRARY_NAME,,}"
SVN_REPO_NAME="${LOWER_LIBRARY_NAME}-svn"
GIT_REPO_NAME="${LOWER_LIBRARY_NAME}-git"

if [[ $(git status --porcelain) ]]; then
  echo "Looks like you have pending changes. Please commit or remove them before continueing"
  exit 1
fi

# Step 1: Update .git/config
CONFIG_FILE=".git/config"
echo "Configuring new library svn repo"
echo "[svn-remote \"${LOWER_LIBRARY_NAME}\"]" >> "${CONFIG_FILE}"
echo "	url = ${SVN_REPO}" >> "${CONFIG_FILE}"
echo "	fetch = ${SVN_BRANCH_NAME}:refs/remotes/${SVN_REPO_NAME}" >> "${CONFIG_FILE}"

# Step 2: Fetch latest from svn
REVISION_FLAG=''
if [ ! -z "${LATEST_REVISION}" ]; then
  REVISION_FLAG="-${LATEST_REVISION}:HEAD"
fi
echo "Loading the latest code for the library"
git svn fetch "${LOWER_LIBRARY_NAME}" "${REVISION_FLAG}"

# Step 3: Setup Git Branch
echo "Setting up git copy"
git checkout -b "${GIT_REPO_NAME}" "${SVN_REPO_NAME}"
git push -u origin "${GIT_REPO_NAME}"
git checkout "${CURRENT_BRANCH_NAME}"

# Step 4: Add Library
echo "Adding submodule for library"
git submodule add -b "${GIT_REPO_NAME}" "git@gitlab.com:alexriedl/action-bar-sets.git" "Libs/${LIBRARY_NAME}"
git add -A
git commit -m "Added ${LIBRARY_NAME} Library"

# Step 5: Update Libs.xml
# TODO:
