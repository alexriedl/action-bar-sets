----------------------
--  Global Helpers  --
----------------------

local function PrintColor(msg, r, g, b)
  DEFAULT_CHAT_FRAME:AddMessage(msg, r, g, b);
end
local function Print(msg)
  PrintColor(msg, 1, 1, 1);
end
local function PrintError(msg)
  PrintColor(msg, 1, 0, 0);
end
local function PrintFetalError(msg)
  _ERRORMESSAGE(msg);
end

local function StringNullOrEmpty(value)
  return value == nil or value == "";
end
local function StringDefault(value, default)
  if(StringNullOrEmpty(value)) then
    return default;
  else
    return value;
  end
end

local function PrintObject(object, tabDepth, recursive)
  if(recursive == nil) then
    recursive = true;
  end

  if(not object or object == nil or type(object) ~= "table") then
    return "nil";
  end
  if(not tabDepth or tabDepth == nil) then
    tabDepth = 0;
  end

  local indent = "";
  for index = 1, tabDepth do
    indent = indent .. " ";
  end

  for key, value in pairs(object) do
    local name = _G and _G[key] or key;
    if(value and value ~= nil and type(value) == "table") then
      print(indent .. name .. ": {");
      if(recursive) then
        PrintObject(value, tabDepth + 2);
      else
        print("...");
      end
      print(indent .. "}");
    else
      print(indent .. name .. ":" .. value);
    end
  end
end


ActionBarSets = {
  print = Print,
  printc = PrintColor,
  printe = PrintError,
  printf = PrintFetalError,
  StringNullOrEmpty = StringNullOrEmpty,
  StringDefault = StringDefault,
  PrintObject = PrintObject,
};
