-- Global Saved Variables: ActionBarSets_SavedSets,ActionBarSets_SetStore

local MIN_ACTION_BAR_ID = 1;
local MAX_ACTION_BAR_ID = 10;
local MIN_ACTION_BAR_SLOT_ID = 1;
local MAX_ACTION_BAR_SLOT_ID = 12;

-- NOTE: This only includes the player's bags - not the bank. Making this the only fucking 0 based list in all of lua
local MIN_BAG_SLOT_ID = 0;
local MAX_BAG_SLOT_ID = 4;

local print = ActionBarSets.print;
local printc = ActionBarSets.printc;
local printe = ActionBarSets.printe;
local printf = ActionBarSets.printf;
local StringNullOrEmpty = ActionBarSets.StringNullOrEmpty;
local StringDefault = ActionBarSets.StringDefault;
local PrintObject = ActionBarSets.PrintObject;

-- Libraries
local AceGUI = LibStub("AceGUI-3.0")

local function GetGlobalSlotId(barId, relativeSlotId)
  -- 1 based logic is dumb............. why lua. why do you do this to me
  local slotId = (barId - 1) * MAX_ACTION_BAR_SLOT_ID + relativeSlotId;
  return slotId;
end

local UIInfo;

local function ReleaseWindow(widget)
  if(not UIInfo) then
    return;
  end

  local frame = UIInfo.Frame;
  AceGUI:Release(widget);
  UIInfo.Frame = nil;
end

local function OpenWindow(name)
  if(not UIInfo) then
    UIInfo = {
      Frame = nil,
    };
  end

  local uiFrame = UIInfo.Frame;
  if(uiFrame) then
    return;
  end

  if(StringNullOrEmpty(name)) then
    printe("Unable to load ActionBarSet with no name");
    return;
  end

  local set = ActionBarSets_SetStore.sets[name]
  if(not set or set == nil) then
    printe("No set saved with the name " .. name);
    return;
  end

  UIFrame = AceGUI:Create("Frame");
  UIFrame:SetTitle("Example Frame");
  UIFrame:SetStatusText("AceGUI-3.0 Example Container Frame");
  UIFrame:SetCallback("OnClose", function(widget) ReleaseWindow(widget); end);
  UIFrame:SetLayout("Flow");

  for barId = MIN_ACTION_BAR_ID, MAX_ACTION_BAR_ID, 1 do
    local barName = set.bars[barId];
    local bar = ActionBarSets_SetStore.bars[barName];

    local uiGroup = AceGUI:Create("InlineGroup")
    uiGroup:SetLayout("Flow");
    UIFrame:AddChild(uiGroup);

    if(bar and bar ~= nil) then
      for slotId = MIN_ACTION_BAR_SLOT_ID, MAX_ACTION_BAR_SLOT_ID, 1 do
        local slot = bar.slots[slotId];

        local slotIcon;
        --[[
        if(slot) then
          slotIcon = AceGUI:Create("Icon")
          slotIcon:SetImage(slot.icon);
          slotIcon:SetImageSize(20, 20);
        else
          slotIcon = AceGUI:Create("InlineGroup")
        end
        --]]

        slotIcon = AceGUI:Create("InteractiveLabel");
        slotIcon:SetImageSize(20, 20);
        if(slot) then
          slotIcon:SetImage(slot.icon);
        else
          slotIcon:SetImage([[Interface\DialogFrame\UI-DialogBox-Background-Dark]]);
        end

        slotIcon:SetHighlight(0.5,0.5,0.5,1);

        slotIcon:SetWidth(21);
        slotIcon:SetHeight(21);
        slotIcon:SetCallback("OnClick", function(button) print("IconClicked"); end);
        uiGroup:AddChild(slotIcon);
      end
    end
  end
end

-------------------------
-- Load Action Bar Set --
-------------------------
local function ClearActionBarSlot(absoluteSlotId)
  if(not HasAction(absoluteSlotId)) then
    return
  end
  PickupAction(absoluteSlotId);
  ClearCursor();
end

local function ApplyToActionBarSlot(absoluteSlotId, info, itemLocationCache)
  if(info == nil) then
    ClearActionBarSlot(absoluteSlotId);
    return;
  end

  local currentType, currentId, currentSubtype = GetActionInfo(absoluteSlotId);
  if(currentType == info.type and currentId == info.id) then
    return;
  end

  if(info.type == "spell") then
    PickupSpell(info.id);
    PlaceAction(absoluteSlotId);
  elseif(info.type == "macro") then
    -- The id is the offset into the macro book. If the user added/removed any
    -- macros the offset may be different. We will just use the macro name and
    -- *hope* it works
    PickupMacro(info.macroName);
    -- TODO: If we failed to pickup a macro, let the user know
    PlaceAction(absoluteSlotId);
  elseif(info.type == "item") then
    if(itemLocationCache ~= nil) then
      local cache = itemLocationCache[info.id];
      if(cache == nil) then
        -- TODO: If the item is currently equipt, the cache will always return null
        itemName, _, _, _, _, _, _, _, _, _, _ = GetItemInfo(info.id);
        if(StringNullOrEmpty(itemName)) then
          itemName = "UNKNOWN";
        end
        printe("Item "..itemName.." ("..info.id..") is not in your inventory. Unable to create Actions for items you don't currently have in your inventory");
        ClearActionBarSlot(absoluteSlotId);
      else
        if(cache.type == "inventory") then
          local inventorySlotId = cache.slotId;
          PickupInventoryItem(inventorySlotId);
          PlaceAction(absoluteSlotId);
        elseif(cache.type == "container") then
          local bagId = cache.bagId;
          local bagSlotId = cache.bagSlotId;
          PickupContainerItem(bagId, bagSlotId);
          PlaceAction(absoluteSlotId);
        else
          -- TODO: WTF did you do!?!?!
          printe("Was asked to load an invalid slot type...");
          ClearActionBarSlot(absoluteSlotId);
        end
      end
    else
      printe("Item cache failed to load. This sounds like a bug in the addon.");
    end
  else
    local id = StringDefault(info.id, "");
    local subtype = StringDefault(info.subtype, "");
    printe("ActionBarSets doesnt know how to place actionbar items of type '"..info.type.."' INFO - ID: '"..id.."' - SubType: '"..subtype.."'");
  end

  ClearCursor();
end

local function BuildItemLocationCache()
  local cache = {};

  for bag = MIN_BAG_SLOT_ID,MAX_BAG_SLOT_ID do
    for slot = 1,GetContainerNumSlots(bag) do
      local itemId = GetContainerItemID(bag, slot);
      if(itemId ~= nil) then
        cache[itemId] = {
          type = "container",
          bagId = bag,
          bagSlotId = slot,
        };
      end
    end
  end

  for slotId = 0,23,1 do
    local itemId = GetInventoryItemID("player", slotId);
    if(itemId ~= nil and cache[itemId] == nil) then
      cache[itemId] = {
        type = "inventory",
        slotId = slotId,
      };
    end
  end

  return cache;
end

local function LoadActionBarSet(name)
  if(StringNullOrEmpty(name)) then
    printe("Unable to load ActionBarSet with no name");
    return;
  end

  local set = ActionBarSets_SetStore.sets[name]
  if(not set or set == nil) then
    printe("No set saved with the name " .. name);
    return;
  end

  print("Loading ActionBarSet " .. name);

  local itemLocationCache = nil;
  ClearCursor();
  for barId = MIN_ACTION_BAR_ID, MAX_ACTION_BAR_ID, 1 do
    local barName = set.bars[barId];
    local bar = ActionBarSets_SetStore.bars[barName];

    if(bar and bar ~= nil) then
      for slotId = MIN_ACTION_BAR_SLOT_ID, MAX_ACTION_BAR_SLOT_ID, 1 do
        local slot = bar.slots[slotId];
        if(slot ~= nill and slot.type == "item" and itemLocationCache == nil) then
          itemLocationCache = BuildItemLocationCache();
        end
        local globalSlotId = GetGlobalSlotId(barId, slotId);
        ApplyToActionBarSlot(globalSlotId, slot, itemLocationCache);
      end
    end
  end

  print("ActionBarSet loaded.");
end

-------------------------
-- Save Action Bar Set --
-------------------------
local function AllocateNewSet(name)
  if(ActionBarSets_SetStore.sets[name] ~= nil) then
    -- TODO: Handle this case somehow
    -- ERROR ??? maybe not???
  end

  local set = {
    name = name,
    bars = {},
  };
  ActionBarSets_SetStore.sets[name] = set;
  return set;
end

local function AllocateNewBar(name)
  if(ActionBarSets_SetStore.bars[name] ~= nil) then
    -- TODO: Handle this case somehow
    -- ERROR ??? maybe not???
  end

  local bar = {
    name = name,
    slots = {},
  };
  ActionBarSets_SetStore.bars[name] = bar;
  return bar;
end

local function AllocateNewBarSlotFromActiveActionBar(slotId)
  if(not HasAction(slotId)) then
    return nil;
  end

  local type, id, subtype = GetActionInfo(slotId);
  local texture = GetActionTexture(slotId);
  result = {
    icon = texture,
    type = type,
    id = id,
    subtype = subtype,
  };

  -- INFO: If any additional type specific information is required, add it below
  if(type == "macro") then
    local name, icon, body, isLocal = GetMacroInfo(id);
    result.macroName = name;
  end

  return result;
end

local function SaveActionBarSet(name)
  if(StringNullOrEmpty(name)) then
    printe("Failed to save ActionBarSet because no name was provided");
    return;
  end

  print("Saving ActionBarSet " .. name);
  local set = AllocateNewSet(name);
  for barId = MIN_ACTION_BAR_ID, MAX_ACTION_BAR_ID, 1 do
    local barName = name .. "_" .. barId;
    local bar = AllocateNewBar(barName);
    set.bars[barId] = barName;
    for relativeSlotId = MIN_ACTION_BAR_SLOT_ID, MAX_ACTION_BAR_SLOT_ID, 1 do
      local globalSlotId = GetGlobalSlotId(barId, relativeSlotId);
      local slot = AllocateNewBarSlotFromActiveActionBar(globalSlotId);
      bar.slots[relativeSlotId] = slot;
    end
  end
  print("ActionBarSet saved.");
end

-----------
-- Extra --
-----------
local function Clear()
  for slotId = 1,MAX_ACTION_BAR_ID*MAX_ACTION_BAR_SLOT_ID,1 do
    ApplyToActionBarSlot(slotId, nil);
  end
end

local function ListSavedActionBarSets()
  local title = "|c00997700";
  local info = "|c00ffffff";
  printc("Saved ActionBarSets:", 1, 0.8, 0);
  for key, set in pairs(ActionBarSets_SetStore.sets) do
    print(title .. key .. info);
  end
end

local function DeleteActionBarSet(name)
  print("Deleting ActionBarSet " .. name);
  ActionBarSets_SetStore.sets[name] = nil;
end

---------------------------
--   Migration Scripts   --
---------------------------
local function MigrateFromSavedSets()
  if(ActionBarSets_SavedSets ~= nil) then
    print("Migrating old sets to new format");
    PrintObject(ActionBarSets_SavedSets, 0, false);
    for setName, set in pairs(ActionBarSets_SavedSets) do
      if(StringNullOrEmpty(setName) or set == nil or set.actionBarSlots == nil) then
        printe("found invalid set, Skipping");
      else
        print("Migrating set " .. setName);
        local newSet = AllocateNewSet(setName);
        local oldSet = set.actionBarSlots;
        for barId = MIN_ACTION_BAR_ID, MAX_ACTION_BAR_ID, 1 do
          local barName = setName .. "_" .. barId;
          local bar = AllocateNewBar(barName);
          newSet.bars[barId] = barName;
          for slotId = MIN_ACTION_BAR_SLOT_ID, MAX_ACTION_BAR_SLOT_ID, 1 do
            local oldSlotId = GetGlobalSlotId(barId, slotId);
            local oldSlot = oldSet[oldSlotId];
            if(oldSlot ~= nil) then
              local newSlot = {
                icon = nil, -- TODO: Maybe do a lookup somehow and store the actual icon?
                type = oldSlot.type,
                id = oldSlot.id,
                subtype = oldSlot.subtype,

                -- NOTE: I guess just always map special fields, because if they are nil, they wont actually be persisted
                macroName = oldSlot.macroName
              };
              bar.slots[slotId] = newSlot;
            end
          end
        end
      end
    end
  end
  ActionBarSets_SavedSets = nil;
end

---------------------------
-- Global Event Handlers --
---------------------------
SLASH_ACTIONBARSETS1, SLASH_ACTIONBARSETS2 = '/actionbarsets', '/abs';
local function ChatHandler(msg, editbox)
  if(StringNullOrEmpty(msg)) then
    local title = "|c00997700";
    local info = "|c00ffffff";
    print(title.."Usage: "..info.."/actionbarsets (/abs)");
    print(title.." * `/abs save <NAME>`"..info.." - Save your current ActionBar layout to the name specified");
    print(title.." * `/abs load <NAME>`"..info.." - Load the saved ActionBarSet and change your current layout to the one that is being loaded");
    print(title.." * `/abs delete <NAME>`"..info.." - Delete the saved ActionBarSet. This does not affect which actions are currently on your action bars");
    print(title.." * `/abs list`"..info.." - List the names of each of the ActionBarSets currently saved");
    print(title.." * `/abs clear`"..info.." - Clear out your action bars");
    return;
  end

  local _, _, cmd, args = string.find(msg, "%s?(%w+)%s?(.*)")
  if(cmd == "list") then
    ListSavedActionBarSets();
  elseif(cmd == "save") then
    if(StringNullOrEmpty(args)) then
      printe("To save an action bar set, you must assign it a name. ie. /abs save my_list");
      return;
    end
    SaveActionBarSet(args);
  elseif(cmd == "load") then
    if(StringNullOrEmpty(args)) then
      printe("To load an action bar set, you must specify which set to load. ie. /abs load my_list");
      return;
    end
    LoadActionBarSet(args);
  elseif(cmd == "delete") then
    if(StringNullOrEmpty(args)) then
      printe("To delete an action bar set, you must specify which set to delete. ie. /abs delete my_list");
      return;
    end
    DeleteActionBarSet(args);
  elseif(cmd == "clear") then
    Clear();
  elseif(cmd == "print") then
    print("SetStore:");
    PrintObject(ActionBarSets_SetStore);
  elseif(cmd == "open") then
    if(StringNullOrEmpty(args)) then
      printe("To load an action bar set, you must specify which set to load. ie. /abs load my_list");
      return;
    end
    OpenWindow(args);
  elseif(cmd == "hardreset") then
    print("Reseting all data");
    ActionBarSets_SetStore = {
      bars =  {},
      sets =  {},
      active_set =  nil,
      schema_version =  0.1
    };
  else
    printe("Unknown ActionBarSets command! Type '/abs' for more information");
  end
end
SlashCmdList["ACTIONBARSETS"] = ChatHandler;

-- Set Store Schema:
-- SetStore = {
--   bars: Bar[],
--   sets: Set[],
--   active_set: number,
--   schema_version: number,
-- }
--
-- Bar: {
--   name: string,
--   slots: Slot[12],
-- }
--
-- Slot: {
--   icon: string,
--   type: enum (item, spell, etc)
--   id: number
--   subtype: string
-- }
--
-- Set: {
--   name: string,
--   bars: string[10], -- Index references into the main SetStore.bars array
-- }


local frame = CreateFrame("FRAME");
frame:RegisterEvent("ADDON_LOADED");
function frame:OnEvent(event, arg1)
  if(event == "ADDON_LOADED") then
    if(arg1 == "ActionBarSets") then
      if(ActionBarSets_SetStore == nil) then
        ActionBarSets_SetStore = {
          bars           = {},
          sets           = {},
          active_set     = nil,
          schema_version = 0.1
        };
        print("Initializing abs SetStore");
      end
      if(ActionBarSets_SavedSets ~= nil) then
        MigrateFromSavedSets();
      end
    end
  end
end
frame:SetScript("OnEvent", frame.OnEvent);
